import React, { useEffect, useState } from "react";
import CreateIcon from "@mui/icons-material/Create";
import { Button } from "@mui/material";

import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import DiscountIcon from "@mui/icons-material/Discount";
import CurrencyRupeeIcon from "@mui/icons-material/CurrencyRupee";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import EventAvailableIcon from "@mui/icons-material/EventAvailable";
import DescriptionIcon from "@mui/icons-material/Description";
import PublishedWithChangesIcon from "@mui/icons-material/PublishedWithChanges";
// import LocationOnIcon from '@mui/icons-material/LocationOn';
import { NavLink, useParams, useHistory } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import Rate from "./rating";


const Details = () => {
  const [getuserdata, setUserdata] = useState([]);
  console.log(getuserdata);

  const { id } = useParams("");
  console.log(id);

  const history = useHistory();

  const getdata = async () => {
    const res = await fetch(`http://localhost:2000/getuser/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 422 || !data) {
      console.log("error ");
    } else {
      setUserdata(data);
      console.log("get data");
    }
  };

  useEffect(() => {
    getdata();
  }, []);

  const deleteuser = async (id) => {
    const res2 = await fetch(`http://localhost:2000/deleteuser/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const deletedata = await res2.json();
    console.log(deletedata);

    if (res2.status === 422 || !deletedata) {
      console.log("error");
    } else {
      console.log("user deleted");
      history.push("/");
    }
  };

  return (
    <>
    
      <div style={{ backgroundColor: "#DCF7F7" }} className="d-flex flex-row">
        <Card
          className="m-3"
          sx={{ maxWidth: 800 }}
          style={{ backgroundColor: "#DCF7F7" }}
        >
          <CardContent>
              <Button href="/">
          <svg
          xmlns="http://www.w3.org/2000/svg" width="25" height="20" fill="black" class="bi bi-back" viewBox="0 0 16 16">
  <path d="M0 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v2h2a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2v-2H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2z" />
</svg>
</Button>
          <h1
                style={{
                  fontWeight: 400,
                  fontFamily: "Roboto",
                  fontSize: "40px",
                }}
              >
                {getuserdata.book_name}
              </h1>
            <div className="add_btn">
              <NavLink to={`/edit/${getuserdata._id}`}>
                {""}
                <button className="btn btn-primary mx-2">
                  <CreateIcon />
                </button>
              </NavLink>
              <button
                className="btn btn-danger"
                onClick={() => deleteuser(getuserdata._id)}
              >
                <DeleteOutlineIcon />
              </button>
            </div>
            <div className="row">
              
              <div className="left_view col-lg-6 col-md-6 col-12">
                <img
                  src="https://gifimage.net/wp-content/uploads/2017/10/book-flipping-pages-gif-10.gif"
                  style={{ width: 235, height: 215 }}
                  alt="profile"
                />

                <h3 className="mt-3">
                  <MenuBookIcon
                    style={{ marginRight: "5px", color: "brown" }}
                  />{" "}
                  book_name: {getuserdata.book_name}
                </h3>
                <h3 className="mt-3">
                  <DiscountIcon
                    style={{ marginRight: "5px", color: "brown" }}
                  />
                  <span />
                  discount: <span>{getuserdata.discount}</span>
                </h3>
                <p className="mt-3">
                  <CurrencyRupeeIcon
                    style={{ marginRight: "7px", color: "brown" }}
                  />
                  book_price: <span>{getuserdata.book_price}</span>
                </p>
                <p className="mt-3">
                  <BorderColorIcon
                    style={{ marginRight: "7px", color: "brown" }}
                  />
                  auther_name: <span>{getuserdata.auther_name}</span>
                </p>
              </div>
              <div className="right_view  col-lg-6 col-md-6 col-12">
                <p className="mt-5">
                  <EventAvailableIcon
                    style={{ marginRight: "7px", color: "brown" }}
                  />
                  availability: <span> {getuserdata.availability}</span>
                </p>
                <p className="mt-3">
                  <PublishedWithChangesIcon
                    style={{ marginRight: "7px", color: "brown" }}
                  />
                  publisher_name: <span>{getuserdata.publisher_name}</span>
                </p>
                <p className="mt-3">
                  <DescriptionIcon
                    style={{ marginRight: "5px", color: "brown" }}
                  />
                  Description: <span>{getuserdata.book_description}</span>
                </p>
              </div>
            </div>
          </CardContent>
        </Card>
        <Card
          className="m-3"
          sx={{ width:"40%" }}
          style={{ backgroundColor: "#DCF7F7" }}
        >
          <CardContent>
            <div>
              <h1 style={{textAlign:"center"}}>{getuserdata.book_name}</h1>
              <h5 style={{textAlign:"center"}}>Description:</h5>
              <p>
                This Slytherin House Edition of Harry Potter and the Half-Blood
                Prince celebrates the noble character of the Hogwarts house
                famed for its pride, ambition and cunning.
              </p>
              <p>
              Harry's sixth year at
                Hogwarts is packed with more great Slytherin moments and
                characters - not least Professor Slughorn's return to Hogwarts
                and the unveiling of Tom Riddle's past. 
                </p>
                <Rate/>
            </div>
          </CardContent>
        </Card>
      </div>
    </>
  );
};

export default Details;
