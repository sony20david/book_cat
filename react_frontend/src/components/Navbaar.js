import React, { useContext, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { searchDataPro } from './context/ContextProvider';
import bookHome from './bookHome/bookHome';

const Navbaar = () => {
    const {searchData, setSearchData} = useContext(searchDataPro);
    const [search,setSearch] = useState("")
    const submitingSearch = (event)=>{
        event.preventDefault()
        setSearchData(search)
    }
    const searchInput = (event)=>{
        setSearch(event.target.value)
    }

    return(
    
       
        <header>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    <NavLink className="navbar-brand" to="/">CRUD APP</NavLink>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="/bookHome">Home</a>
                            </li>
                        
                        </ul>
                        <form className="d-flex" onSubmit={submitingSearch}>
                            <input className="form-control me-2" type="search" onChange={searchInput} placeholder="Search" aria-label="Search" />
                            <button className ="btn btn-outline-success" type ="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>
        </header>
    
    )
}

export default Navbaar
