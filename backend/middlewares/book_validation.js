import { request, response } from "express";

/**
 *
 * @param request request from the client(postman)
 * @param response sends response to the client
 * @param next Callback argument to the middleware function, called "next" by convention.
 * checks for the valid params =>goes to valid(register_data) function
 * if the return status code is 200 goes to next middleware
 * else displays error message
 */
function valid(body_params) {
  if (Object.keys(body_params).length !== 7) {
    //if required number of params are not there it will send 421 status code
    return {
      status_code: 406,
      status_message: "Parameters count is not matches with database fields",
    };
  }
  let body_keys_list = [];
  for (let item in body_params) {
    body_keys_list.push(item);
  }
  //delaring a list of keys to validate the keys
  let key_list = [
   "book_name",
   "book_description",
   "book_price",
   "discount",
   "availability",
   "auther_name",
   "publisher_name",

    
  ];
  for (let key of key_list) {
    if (body_keys_list.includes(key)) {
      continue;
    } else {
      let msg = key + " is Missing!";
      return { status_code: 406, status_message: msg };
    }
  }
  return { status_code: 200 };
}

/**
 *
 * @param request  request from the client(postman)
 * @param response response to the client based on the request
 * @param next if register_param_count_checker satisfies all the conditions then goes to
 * next()
 */
function books_param_count_checker(request, response, next) {
  const books_data= request.body;
  const validate_params = valid(books_data);
  if (validate_params.status_code !== 200) {
    console.log(validate_params.status_code);
    response.send(JSON.stringify(validate_params));
  } else {
    next();
  }
}


export default books_param_count_checker


