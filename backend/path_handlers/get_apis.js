import { request } from "express";
import { CREATE_BOOK_DETAILS_DATA } from "../utils/model";

const get_books_details_handler = async(req, res) => {
  //fetching get_profils details for getting the all employee details
  try {
    const userdata = await CREATE_BOOK_DETAILS_DATA.find({});
    console.log("userdata",userdata);
    res.send(userdata)
    
} catch (error) {
    res.status(422).json(error);
}
};

const get_books_specific_handler=(request, response) => {
  const {search}=request.headers
  console.log(request.headers)
  console.log(search)
  //fetching get_profils details for getting the all employee details
  CREATE_BOOK_DETAILS_DATA.find({book_name:search}, (err, data) => {
    // any error occur in the data fetching it prints in the console
    if (err) {
      // printing error in console
      console.log(err);
    } //if there is no error in the data fetching it goes through the else block
    else {
      //it sends the data
      response.send(data);
    }
  });
};

const get_users_by_id=async(request,response)=>{
  try {
      console.log(request.params);
      const {id} = request.params;

      const userindividual = await CREATE_BOOK_DETAILS_DATA.findById({_id:id});
      console.log(userindividual);
      response.status(201).json(userindividual)

  } catch (error) {
      response.status(422).json(error);
  }
}


const get_old_details_handler= (request,response)=>{
  let data = CREATE_BOOK_DETAILS_DATA.findOne().sort({_id:1}).limit(3)
  .then((data) => {
      console.log(data)
      response.send(data)
  })
  .catch((err) => {
      console.log(err)
      response.status(400).send('Error')
  })
};

const get_latest_details_handler= (request,response)=>{
  let data = CREATE_BOOK_DETAILS_DATA.findOne().sort({_id:-1}).limit(3)
  .then((data) => {
      console.log(data)
      response.send(data)
  })
  .catch((err) => {
      console.log(err)
      response.status(400).send('Error')
  })
};


export  {get_books_details_handler,get_latest_details_handler,get_old_details_handler,get_books_specific_handler,get_users_by_id};