import { response } from "express";
import { request } from "express";
import { CREATE_BOOK_DETAILS_DATA } from "../utils/model";

const post_books_data_handler=async(req,res)=>{
  const {book_name,publisher_name,book_price,discount,availability,auther_name,book_description} = req.body;

    if(!book_name || !publisher_name || !book_price || !discount || !availability || !auther_name || !book_description){
        res.status(422).json("plz fill the data");
    }

    try {
        
        const preuser = await CREATE_BOOK_DETAILS_DATA.findOne({book_name:book_name});
        console.log(preuser);

        if(preuser){
            res.status(422).json("this is user is already present");
        }else{
            const adduser = new CREATE_BOOK_DETAILS_DATA({
              book_name,publisher_name,book_price,discount,availability,auther_name,book_description
            });

            await adduser.save();
            res.status(201).json(adduser);
            console.log(adduser);
        }

    } catch (error) {
        res.status(422).json(error);
    }
  }


export default post_books_data_handler;