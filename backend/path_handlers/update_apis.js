import { CREATE_BOOK_DETAILS_DATA } from "../utils/model";
const update_path_handler= async(req,res)=>{
  try {
      const {id} = req.params;

      const updateduser = await CREATE_BOOK_DETAILS_DATA.findByIdAndUpdate(id,req.body,{
          new:true
      });

      console.log(updateduser);
      res.status(201).json(updateduser);

  } catch (error) {
      res.status(422).json(error);
  }
}

export default update_path_handler;


